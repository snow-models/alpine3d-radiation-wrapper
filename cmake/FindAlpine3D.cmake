INCLUDE(LibFindMacros)

IF(WIN32)
	GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT1 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Alpine3d;UninstallString]" PATH CACHE INTERNAL)
	GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT2 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Alpine3d;UninstallString]" PATH CACHE INTERNAL)
	GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT3 "[HKEY_LOCAL_MACHINE\\SOFTWARE\\WSL Institute for Snow and Avalanche Research\\Alpine3d]" ABSOLUTE CACHE INTERNAL)
	GET_FILENAME_COMPONENT(METEOIO_ROOT4 "C:/Progra~1/Alpine3d*" ABSOLUTE CACHE INTERNAL)
	SET(SEARCH_PATH
		ENV LIB
		./lib ./bin
		./lib/Debug ./bin/Debug
		./lib/Release ./bin/Release
		../../lib ../../bin
		../../lib/Debug ../../bin/Debug
		../../lib/Release ../../bin/Release
		${SRC_DIR}/lib ${SRC_DIR}/bin
		${SRC_DIR}/lib/Debug ${SRC_DIR}/bin/Debug
		${SRC_DIR}/lib/Release ${SRC_DIR}/bin/Release
		${SRC_DIR}/../../lib ${SRC_DIR}/../../bin
		${SRC_DIR}/../../lib/Debug ${SRC_DIR}/../../bin/Debug
		${SRC_DIR}/../../lib/Release ${SRC_DIR}/../../bin/Release
		${SRC_DIR}/bin ${SRC_DIR}/lib
		${LIBALPINE3D_ROOT1}/bin ${LIBALPINE3D_ROOT1}/lib
		${LIBALPINE3D_ROOT2}/bin ${LIBALPINE3D_ROOT2}/lib
		${LIBALPINE3D_ROOT3}/bin ${LIBALPINE3D_ROOT3}/lib
		${LIBALPINE3D_ROOT4}/bin ${LIBALPINE3D_ROOT4}/lib )

	IF(MSVC)
		FIND_LIBRARY(LIBALPINE3D_LIBRARY
			NAMES libalpine3d.lib
			HINTS ${SEARCH_PATH}
			DOC "Location of the libalpine3d, like c:/Program Files/Alpine3d-2.0.0/lib/libalpine3d.lib"
			)
	ELSE(MSVC)
		FIND_LIBRARY(LIBALPINE3D_LIBRARY
			NAMES libalpine3d.dll.a libalpine3d.a
			HINTS ${SEARCH_PATH}
			DOC "Location of the libalpine3d, like c:/Program Files/Alpine3d-2.0.0/lib/libalpine3d.dll.a"
			)
	ENDIF(MSVC)
ELSE(WIN32)
	IF(APPLE)
		FIND_LIBRARY(LIBALPINE3D_LIBRARY
		NAMES alpine3d
		HINTS
			ENV LD_LIBRARY_PATH
			ENV DYLD_FALLBACK_LIBRARY_PATH
			./lib
			../../lib
			../../../lib
			${SRC_DIR}/lib
			"~/usr/lib"
			"/Applications/Alpine3d/lib"
			"/usr/local/lib"
			"/usr/lib"
			"/opt/lib"
		DOC "Location of the libalpine3d, like /usr/lib/libalpine3d.dylib"
		)
	ELSE(APPLE)
		FIND_LIBRARY(LIBALPINE3D_LIBRARY
		NAMES alpine3d
		HINTS
			ENV LD_LIBRARY_PATH
			./lib
			../../lib
			../../../lib
			${SRC_DIR}/lib
			"~/usr/lib"
			"/usr/local/lib"
			"/usr/lib"
			"/opt/lib"
		DOC "Location of the libalpine3d, like /usr/lib/libalpine3d.so"
		)
	ENDIF(APPLE)
ENDIF(WIN32)

IF(LIBALPINE3D_LIBRARY)
	#build LIBALPINE3D_ROOT so we can provide a hint for searching for the header file
	IF(${CMAKE_VERSION} VERSION_GREATER "2.8.11")
		GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT ${LIBALPINE3D_LIBRARY} DIRECTORY) #get PATH
		GET_FILENAME_COMPONENT(MSVC_TARGET ${LIBALPINE3D_ROOT} NAME) #special directory name for some MSVC
		IF(("${MSVC_TARGET}" STREQUAL "Debug") OR ("${MSVC_TARGET}" STREQUAL "Release"))
			GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT ${LIBALPINE3D_ROOT} DIRECTORY) #go up one level
		ENDIF(("${MSVC_TARGET}" STREQUAL "Debug") OR ("${MSVC_TARGET}" STREQUAL "Release"))
		GET_FILENAME_COMPONENT(LIBALPINE3D_ROOT ${LIBALPINE3D_ROOT} DIRECTORY) #go up one level
	ELSE(${CMAKE_VERSION} VERSION_GREATER "2.8.11")
		GET_FILENAME_COMPONENT(alpine3d_libs_root ${LIBALPINE3D_LIBRARY} PATH)
		SET(LIBALPINE3D_ROOT "${alpine3d_libs_root}/../")
		STRING(REPLACE  " " "\\ " LIBALPINE3D_ROOT ${LIBALPINE3D_ROOT})
	ENDIF(${CMAKE_VERSION} VERSION_GREATER "2.8.11")


	# locate main header file
	FIND_PATH(LIBALPINE3D_INCLUDE_DIR
	NAMES alpine3d/Alpine3D.h
	HINTS
		"${LIBALPINE3D_ROOT}/include"
		"${LIBALPINE3D_ROOT}"
		"~/usr/include"
		"/usr/local/include"
		"/usr/include"
		"/opt/include"
	DOC "Location of the libalpine3d headers, like /usr/include"
	)
ENDIF(LIBALPINE3D_LIBRARY)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
SET(LIBALPINE3D_PROCESS_INCLUDES LIBALPINE3D_INCLUDE_DIR)
SET(LIBALPINE3D_PROCESS_LIBS LIBALPINE3D_LIBRARY)
libfind_process(LIBALPINE3D)
