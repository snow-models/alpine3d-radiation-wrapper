#!/bin/bash

MPI_WORKER=1
export OMP_NUM_THREADS=8

MPI="mpiexec -n ${MPI_WORKER}"

EXE="${MPI} ../bin/a3d_radiation_wrapper"

mkdir -p output/grids

CMD="${EXE} \
--iofile=./io.ini \
--np-ebalance=${OMP_NUM_THREADS}"

${CMD}
