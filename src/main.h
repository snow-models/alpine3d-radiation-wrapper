#include <alpine3d/Alpine3D.h>
#include <meteoio/MeteoIO.h>
#include <snowpack/libsnowpack.h>

using namespace std;
using namespace mio;

void readParams(int argc, char **argv, map<string, string> &parameters);

void run_simulation(const string iofile, int nebalance);

void compute_radiation(TerrainRadiationAlgorithm *terrain_radiation,
                       const DEMObject &dem, double albedo_value,
                       double direct_value, double diffuse_value,
                       double solarAzimuth, double solarElevation,
                       Grid2DObject &albedo, Array2D<double> &direct,
                       Array2D<double> &diffuse, Array2D<double> &reflected,
                       Array2D<double> &view_factor);

void compute_shading_and_projection(const DEMObject &dem,
                                    Array2D<double> &direct,
                                    double solarAzimuth, double solarElevation);

void writeOutput(IOManager &io, const DEMObject &dem,
                 const Array2D<double> &direct, const Array2D<double> &diffuse,
                 const Array2D<double> &reflected,
                 const Array2D<double> &view_factor, const string &algo,
                 double albedo_value, double direct_value, double diffuse_value,
                 double solarAzimuth, double solarElevation);
