#include "main.h"
#include <iostream>
#include <sstream>

using namespace std;
using namespace mio;

int main(int argc, char **argv) {

  // Read input parameters
  map<string, string> parameters;
  readParams(argc, argv, parameters);

  // Set parameters needed
  std::string iofile("io.ini");
  int nebalance = 0;
  auto it = parameters.find("iofile");
  if (it != parameters.end())
    iofile = it->second;
  it = parameters.find("np-ebalance");
  if (it != parameters.end()) {
    std::stringstream ss;
    ss << it->second;
    ss >> nebalance;
  }

  // Do work
  run_simulation(iofile, nebalance);
  return 0;
}

void readParams(int argc, char **argv, map<string, string> &parameters) {
  for (size_t i = 1; i < argc; i++) {
    string arg(argv[i]);
    size_t position = arg.find("--");
    if (position != 0) {
      throw InvalidNameException("Only long arguments starting with -- are "
                                 "accepted as paremeters. You proveded '" +
                                     arg,
                                 AT);
    }
    position = arg.find("=");
    if (position == string::npos) {
      throw InvalidNameException(
          "Only long arguments and values separated by an = are allowed as "
          "paremeters. You proveded '" +
              arg,
          AT);
    }
    parameters.insert({arg.substr(2, position - 2), arg.substr(position + 1)});
  }
}

void run_simulation(const string iofile, int nebalance) {

  Config cfg(iofile);
  IOManager io(cfg);

  DEMObject dem;
  dem.setDefaultAlgorithm("CORRIPIO");
  dem.setUpdatePpt((DEMObject::update_type)(
      DEMObject::SLOPE | DEMObject::NORMAL | DEMObject::CURVATURE));
  io.readDEM(dem);
  dem.sanitize();
	io.write2DGrid(mio::Grid2DObject(dem.cellsize,dem.llcorner,dem.slope), "DEM_SLOPE");
	io.write2DGrid(mio::Grid2DObject(dem.cellsize,dem.llcorner,dem.azi), "DEM_AZI");
	io.write2DGrid(mio::Grid2DObject(dem.cellsize,dem.llcorner,dem.curvature), "DEM_CURVATURE");

  // Create arrays to store data
  Array2D<double> view_factor(dem.getNx(), dem.getNy(), IOUtils::nodata);
  Grid2DObject albedo{dem, IOUtils::nodata};
  Array2D<double> direct{dem.getNx(), dem.getNy(), IOUtils::nodata};
  Array2D<double> diffuse{dem.getNx(), dem.getNy(), IOUtils::nodata};
  Array2D<double> reflected{dem.getNx(), dem.getNy(), IOUtils::nodata};

  // Create vector of paremeters to loop over
  vector<double> solarElevationVect{8, 30, 60, 90};
  vector<double> albedoVect{0.3, 0.8};
  vector<string> algoVect{"SIMPLE","COMPLEX","HELBIG"};

  TerrainRadiationAlgorithm *terrain_radiation;

  for (const auto &algo : algoVect) {
    cfg.addKey("Terrain_Radiation_Method", "EBALANCE", algo);
    terrain_radiation =
        TerrainRadiationFactory::getAlgorithm(cfg, dem, nebalance);

    for (const auto &albedo_value : albedoVect) {
      for (const auto &solarElevation : solarElevationVect) {

        const double direct_value = 1000;
        const double diffuse_value = 150;
        const double solarAzimuth = 180;

        compute_radiation(terrain_radiation, dem, albedo_value, direct_value,
                          diffuse_value, solarAzimuth, solarElevation, albedo,
                          direct, diffuse, reflected, view_factor);

        writeOutput(io, dem, direct, diffuse, reflected, view_factor, algo,
                    albedo_value, direct_value, diffuse_value, solarAzimuth,
                    solarElevation);
      }
    }
    delete terrain_radiation;
  }
}

void compute_radiation(TerrainRadiationAlgorithm *terrain_radiation,
                       const DEMObject &dem, double albedo_value,
                       double direct_value, double diffuse_value,
                       double solarAzimuth, double solarElevation,
                       Grid2DObject &albedo, Array2D<double> &direct,
                       Array2D<double> &diffuse, Array2D<double> &reflected,
                       Array2D<double> &view_factor) {

  albedo.grid2D = albedo_value;
  direct = direct_value*sin(solarElevation*Cst::to_rad);
  Array2D<double> direct_unshaded_horizontal{direct};
  compute_shading_and_projection(dem, direct, solarAzimuth, solarElevation);
  diffuse = diffuse_value;
  reflected = 0;

  Grid2DObject ta{dem, IOUtils::nodata};
  Grid2DObject rh{dem, IOUtils::nodata};
  Grid2DObject ilwr{dem, IOUtils::nodata};

  terrain_radiation->setMeteo(albedo.grid2D, ta.grid2D, rh.grid2D, ilwr.grid2D);
  terrain_radiation->getSkyViewFactor(view_factor);
  terrain_radiation->getRadiation(direct, diffuse, reflected,
                                  direct_unshaded_horizontal,
                                  solarAzimuth, solarElevation);
}

void compute_shading_and_projection(const DEMObject &dem,
                                    Array2D<double> &direct,
                                    double solarAzimuth,
                                    double solarElevation) {

  const double tan_sun_elev = tan(solarElevation * mio::Cst::to_rad);
  for (size_t jj = 0; jj < dem.getNy(); ++jj) {
    for (size_t ii = 0; ii < dem.getNx(); ++ii) {
      const double tan_horizon =
          mio::DEMAlgorithms::getHorizon(dem, ii, jj, solarAzimuth);
      if (tan_sun_elev < tan_horizon) { // cell is shaded
        direct(ii, jj) = 0.;
      } else {
        const double slope_azi = dem.azi(ii, jj);
        const double slope_angle = dem.slope(ii, jj);
        direct(ii, jj) = mio::SunTrajectory::projectHorizontalToSlope(
	            solarAzimuth, solarElevation, slope_azi, slope_angle,
            direct(ii, jj));
      }
    }
  }
}

void writeOutput(IOManager &io, const DEMObject &dem,
                 const Array2D<double> &direct, const Array2D<double> &diffuse,
                 const Array2D<double> &reflected,
                 const Array2D<double> &view_factor, const string &algo,
                 double albedo_value, double direct_value, double diffuse_value,
                 double solarAzimuth, double solarElevation) {
  // Generate file name
  std::stringstream filename;
  filename << algo;
  filename << "_albedo_";
  filename << albedo_value;
  filename << "_direct_";
  filename << direct_value;
  filename << "_diffues_";
  filename << diffuse_value;
  filename << "_solarAzimuth_";
  filename << solarAzimuth;
  filename << "_solarElevation";
  filename << solarElevation;

  io.write2DGrid(mio::Grid2DObject(dem.cellsize, dem.llcorner, direct),
                 "ISWR_DIR_" + filename.str());
  io.write2DGrid(mio::Grid2DObject(dem.cellsize, dem.llcorner, diffuse),
                 "ISWR_DIF_" + filename.str());
  io.write2DGrid(mio::Grid2DObject(dem.cellsize, dem.llcorner, reflected),
                 "ISWR_TER_" + filename.str());
  io.write2DGrid(mio::Grid2DObject(dem.cellsize, dem.llcorner, view_factor),
                 "VF_" + filename.str());
}
